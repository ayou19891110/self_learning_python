import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import router from './router'

// import axios from 'axios'

// Vue.prototype.$axios = axios
// import requests from './requests'

import './assets/pydoctheme.css'
import './assets/pygments.css'

Vue.config.productionTip = false

Vue.use(ElementUI);

new Vue({
  render: h => h(App),
  router,
  // requests,
}).$mount('#app')
