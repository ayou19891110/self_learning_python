import requests from '@/requests'

/**
* 封装post请求
*/
export function Post(data) {
  return requests({
    url: '/api/python_run',
    method: 'post',
    data: data
  })
}