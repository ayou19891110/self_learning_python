import Vue from 'vue'
import VueRouter from 'vue-router'
import dataone from '@/components/d/dataone';
import datatwo from '@/components/d/datatwo';
import datathree from '@/components/d/datathree';
import datafour from '@/components/d/datafour';
import datafive from '@/components/d/datafive';
import datasix from '@/components/d/datasix';
import dataseven from '@/components/d/dataseven';
import dataeight from '@/components/d/dataeight';
import datanine from '@/components/d/datanine';
import dataten from '@/components/d/dataten';
import dataeleven from '@/components/d/dataeleven';
import datatwelve from '@/components/d/datatwelve';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [              //配置路由，这里是个数组
    {                    //每一个链接都是一个对象
      path: '/',         //链接路径
      name: 'dataone',     //路由名称，
      component: dataone   //对应的组件模板
    },
    {
      path: '/2',
      name: 'datatwo',
      component: datatwo
    },
    {
      path: '/3',
      name: 'datathree',
      component: datathree
    },
    {
      path: '/4',
      name: 'datafour',
      component: datafour
    },
    {
      path: '/5',
      name: 'datafive',
      component: datafive
    },
    {
      path: '/6',
      name: 'datasix',
      component: datasix
    },
    {
      path: '/7',
      name: 'dataseven',
      component: dataseven
    },
    {
      path: '/8',
      name: 'dataeight',
      component: dataeight
    },
    {
      path: '/9',
      name: 'datanine',
      component: datanine
    },
    {
      path: '/10',
      name: 'dataten',
      component: dataten
    },
    {
      path: '/11',
      name: 'dataeleven',
      component: dataeleven
    },
    {
      path: '/12',
      name: 'datatwelve',
      component: datatwelve
    }
  ]
})