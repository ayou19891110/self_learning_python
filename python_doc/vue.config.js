/**
* 这里的配置和导入模块都是为了解决跨域问题
*/
var proxyMiddleware = require('http-proxy-middleware')

module.exports = {
	// publicPath: process.env.NODE_ENV !== 'production',
    devServer: {
        // open: true,
        // host: 'localhost',
        // port: 8998,
        // https: false,
        //以上的ip和端口是我们本机的;下面为需要跨域的
        proxy: {//配置跨域
            '/api': {
                target: 'http://localhost:5000/api',//这里后台的地址模拟的;应该填写你们真实的后台接口
                ws: true,
                changeOrigin: true,//允许跨域
                pathRewrite: {
                    '^/api': ''//请求的时候使用这个api就可以
                }
            }
        }
    }
}
