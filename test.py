import subprocess, tempfile, os, sys, shutil, json
from flask import Flask
from flask_restful import Api, Resource, request
from flask_cors import *


app = Flask(__name__)
api = Api(app)
CORS(app, supports_credentials=True)

class PythonRun(Resource):

    # 路径分隔符
	SEP = os.path.sep

	# Python解释程序路径
	EXEC = sys.executable

	# 创建临时文件夹
	TEMP = tempfile.mkdtemp(suffix='_py', prefix='temporary_folder_python_')

	# 创建临时文件
	def write_py(self, name, code):
	    fpath = os.path.join(self.TEMP, '%s.py' % name)
	    with open(fpath, 'w', encoding='utf-8') as f:
	        f.write(code)
	    print('Code wrote to: %s' % fpath)  # 代码文件所在目录
	    return fpath

	# 编码
	def decode(self, s):
	    try:
	        return s.decode('utf-8')
	    except UnicodeDecodeError:
	        return s.decode('gbk')

	# 执行python
	def perform_py(self, name, code):
		r = dict()
		fpath = ''
		try:
			fpath = self.write_py(name, code)
			print('Execute: %s %s' % (self.EXEC, fpath))
			r['output'] = self.decode(subprocess.check_output([self.EXEC, fpath], stderr=subprocess.STDOUT, timeout=5))
		except subprocess.CalledProcessError as e:
			r = dict(error='Exception', output=self.decode(e.output))
		except subprocess.TimeoutExpired as e:
			r = dict(error='Timeout', output='执行超时')
		except subprocess.CalledProcessError as e:
			r = dict(error='Error', output='执行错误')
		finally:
			return r

	# 删除临时文件夹
	# def remove_py(self, fpath):
	# 	if self.SEP in fpath:
	# 		if os.path.exists(fpath.split('/test.py')[0]):
	# 			shutil.rmtree(fpath.split('/test.py')[0])
	# 		else:
	# 			os.removedirs(fpath.split('/test.py')[0])

	def get(self):
		""" grt测试"""
		return {'data': 'hello'}

	def post(self):
		""" post请求"""
		data = request.json
		code = data.get('code')
		out_data = self.perform_py('test', code)
		return out_data


api.add_resource(PythonRun, '/api/python_run')

if __name__ == '__main__':
    app.run(debug=True)

