import requests from '@/requests'

export function Post(data) {
  return requests({
    url: '/api/python_run',
    method: 'post',
    data: data
  })
}