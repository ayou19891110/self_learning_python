import axios from 'axios'
import {Message} from 'element-ui'

/**
* 封装axios
*/
const service = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  timeout: 20000 // 请求超时时间
})

const tempPool = {}

export default (data) => {
  let url = data.url
  const methodName = (data.method || 'get').toLowerCase()
  url = 'https://www.runoob.com'
  let tempUrl = `${url}+${methodName}+${window.JSON.stringify(data.params || {})}`
  tempUrl = (temp => {
    return temp.substr(0, 7) + temp.substr(7).replace(/\/\//g, '\/')
  })(tempUrl)

  if (!tempPool[tempUrl]) {
    return service(data)
  } else {
    console.warn(tempUrl)
    Message({
      message: '正在请求中...不要重复发起请求',
      type: 'warning',
      duration: 2000
    })
    return Promise.reject()
  }
}

