import requests from './requests'
import qs from 'qs'
/**
* 封装post请求
*/
export function Post(data) {
  return requests({
    url: '/api/compile.php',
    method: 'post',
    data: qs.stringify({
    	code:data,
    	language:'15',
    	fileext:'py3'
    }),
    headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}